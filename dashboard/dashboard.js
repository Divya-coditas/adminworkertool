setcontainer("headercontainer")
loadComponent("admin")
let logout=()=>{
    setcontainer("modal")
    localStorage.removeItem("token")
    httpclient.settoken()
    authenticate()
    console.log("logout")
}
 document.getElementById("logout").
 addEventListener("click",logout)

document.getElementById("admin-button").addEventListener("click",()=>{
    setcontainer("headercontainer")
    loadComponent("admin")
})

document.getElementById("worker-button").addEventListener("click",()=>{
    setcontainer("headercontainer")
    loadComponent("worker")
})

document.getElementById("order-button").addEventListener("click",()=>{
    setcontainer("headercontainer")
    loadComponent("orders")
})

document.getElementById("tool-button").addEventListener("click",()=>{
    setcontainer("headercontainer")
    loadComponent("tools")
})