let allTools=[];
let editObj;
let createTool=async ()=>{
    if(!editObj)
    {
        const forms=document.forms["createToolForm"];
        const createToolObject={
            toolName:forms["toolName"].value,
            toolSize:forms["toolSize"].value,
            toolPrice:forms["toolPrice"].value}
         await httpclient.post("admin/createTool",createToolObject)
         
        
    }
    else{
        const forms=document.forms["createToolForm"];
        const createToolObject={
            toolId:editObj.toolId,
            toolName:forms["toolName"].value,
            toolSize:forms["toolSize"].value,
            toolPrice:forms["toolPrice"].value}
            await httpclient.put("admin/updateTool",createToolObject)
            editObj=null;
    }
    getTool()
}
let  getTool=async()=>{
    console.log("tools page")
    let response=await httpclient.get("admin/getTools")
    let total=""
    response.forEach(((obj)=>{
         total+=`<tr><td>${obj.toolName}</td><td>${obj.toolSize}</td><td>${obj.toolPrice}</td></tr><td><button class="editButton" data="${obj.toolId}">edit</button></td><td><button class="deleteButton" data="${obj.toolId}">delete</button></td>`
        }))
    allTools=response
    document.getElementById("toolsBody").innerHTML=total;
    for( let each of document.getElementsByClassName("editButton"))
    each.addEventListener('click',editClick)
    for( let each of document.getElementsByClassName("deleteButton"))
    each.addEventListener('click',deleteClick)
    document.getElementById("toolName").value=""
    document.getElementById("toolSize").value=""
    document.getElementById("toolPrice").value=""
    
}
let editClick=($event)=>{
    const id=$event.srcElement.getAttribute('data')
    editObj=allTools.filter(r=>r.toolId==id)[0]
    document.getElementById("toolName").value=editObj.toolName
    document.getElementById("toolSize").value=editObj.toolSize
    document.getElementById("toolPrice").value=editObj.toolPrice
}

let deleteClick=async ($event)=>{
    const id=$event.srcElement.getAttribute('data')
    await httpclient.delete("admin/deleteTool/"+id)
    getTool();
}
getTool();
document.getElementById("createTool").addEventListener('click',createTool)
