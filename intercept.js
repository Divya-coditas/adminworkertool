export class Httpclient {
    constructor()
    {
        this.settoken()
     }
    #baseURL;
    #headers
    setbaseurl(baseURL){
        this.#baseURL=baseURL
    }

async get(URI){
return (await fetch(this.#baseURL+URI,{method:"get",headers:this.#headers
})).json()
}
async post(URI,data){
    return (await fetch(this.#baseURL+URI,{method:"post",body:JSON.stringify(data),headers:{...this.#headers,'Content-Type': 'application/json'
    }})).json()

}
async put(URI,data){
  return (await fetch(this.#baseURL+URI,{method:"put",body:JSON.stringify(data),headers:{...this.#headers,'Content-Type': 'application/json'
  }})).json()

}
async delete(URI,data){
  return fetch(this.#baseURL+URI,{method:"delete",headers:this.#headers})

}
setheaders(headers)
{
    this.#headers={...this.#headers,headers}
}
settoken(){
    const token=localStorage.getItem("token")
      if (token) this.#headers= {
        'ngrok-skip-browser-warning':'1234',
        Authorization:"Bearer "+token 
      } 
      else{
        this.#headers={
          'ngrok-skip-browser-warning':'1234'
        }
      }

    }}