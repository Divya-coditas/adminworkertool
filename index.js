import { Httpclient} from "./intercept.js"
import { authenticate} from "./componentLoader.js"
export const httpclient= new Httpclient()
httpclient.setbaseurl("https://e18b-103-169-241-128.in.ngrok.io/")

authenticate()
function loadJS(FILE_URL, async = true) {
  let scriptEle = document.createElement("script");

  scriptEle.setAttribute("src", FILE_URL);
  scriptEle.setAttribute("type", "text/javascript");
  scriptEle.setAttribute("async", async);

  document.body.appendChild(scriptEle);


  scriptEle.addEventListener("load", () => {
    console.log("File loaded")
  });
  scriptEle.addEventListener("error", (ev) => {
    console.log("Error on loading file", ev);
  });
}


