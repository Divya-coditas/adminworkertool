let allWorkers=[];
let editObj;
let createWorker=async ()=>{
    if(!editObj)
    {
        const forms=document.forms["createWorkerForm"];
        const createWorkerObject={
            workerName:forms["workerName"].value,
            workerUsername:forms["workerUsername"].value,
            workerPassword:forms["workerPassword"].value,
            workerSalary:forms["workerSalary"].value}
         await httpclient.post("admin/createWorker",createWorkerObject)
         
        
    }
    else{
        const forms=document.forms["createWorkerForm"];
        const createWorkerObject={
            workerId:editObj.workerId,
            workerName:forms["workerName"].value,
            workerUsername:forms["workerUsername"].value,
            workerPassword:forms["workerPassword"].value,
            workerSalary:forms["workerSalary"].value}
            await httpclient.put("admin/updateWorker",createWorkerObject)
            editObj=null;
    }
    getWorker()
}
let  getWorker=async()=>{
    let response=await httpclient.get("admin/getWorkers")
    let total=""
    response.forEach(((obj)=>{
         total+=`<tr><td>${obj.workerName}</td><td>${obj.workerUsername}</td><td>${obj.workerPassword}</td><td>${obj.workerSalary}</td></tr><td><button class="editButton" data="${obj.workerId}">edit</button></td><td><button class="deleteButton" data="${obj.workerId}">delete</button></td>`
        }))
    allWorkers=response
    document.getElementById("workersBody").innerHTML=total;
    for( let each of document.getElementsByClassName("editButton"))
    each.addEventListener('click',editClick)
    for( let each of document.getElementsByClassName("deleteButton"))
    each.addEventListener('click',deleteClick)
    document.getElementById("workerName").value=""
    document.getElementById("workerUsername").value=""
    document.getElementById("workerPassword").value=""
    document.getElementById("workerSalary").value=""
    
}
let editClick=($event)=>{
    const id=$event.srcElement.getAttribute('data')
    editObj=allWorkers.filter(r=>r.workerId==id)[0]
    document.getElementById("workerName").value=editObj.workerName
    document.getElementById("workerUsername").value=editObj.workerUsername
    document.getElementById("workerPassword").value=editObj.workerPassword
    document.getElementById("workerSalary").value=editObj.workerSalary
}

let deleteClick=async ($event)=>{
    const id=$event.srcElement.getAttribute('data')
    await httpclient.delete("admin/deleteWorker/"+id)
    getWorker();
}
getWorker();
document.getElementById("createWorker").addEventListener('click',createWorker)
